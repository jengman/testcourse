package ltu;

import static org.junit.Assert.*;
import java.io.IOException;

import org.junit.Test;
import org.junit.Before;

public class PaymentTest
{
  IPayment pay = null;
  @Before
  public void init() throws IOException {
    ICalendar cal = new CalendarImpl();
    pay = new PaymentImpl(cal);
  }

  /**
    Test relaterade till ålder
  */

  @Test
  public void ID101() { // över 20
    assertEquals(9904, pay.getMonthlyAmount("19990412-1234", 0, 100, 100)); // 21-åring ska få fullt lån och bidrag
    assertEquals(4960, pay.getMonthlyAmount("19990412-1234", 0, 50, 100)); // 21-åring ska få halvt lån och bidrag vid 50% plugg

    assertEquals(0, pay.getMonthlyAmount("20000513-1234", 0, 100, 100)); // 19-åring ska inte få fullt lån och bidrag
    assertEquals(0, pay.getMonthlyAmount("20000513-1234", 0, 50, 100)); // 19-åring ska inte få halvt lån och bidrag vid 50% plugg
  }

  @Test
  public void ID102() { // under 56 för bidrag
    assertEquals(2816, pay.getMonthlyAmount("19630101-1234", 0, 100, 100)); // 55-åring ska få fullt bidrag
    assertEquals(0, pay.getMonthlyAmount("19620101-1234", 0, 100, 100)); // 56-åring ska inte få fullt bidrag
  }

  @Test
  public void ID103() { // under 47 för lån
    assertEquals(9904, pay.getMonthlyAmount("19730101-1234", 0, 100, 100)); // 46-åring ska få fullt lån
    assertEquals(2816, pay.getMonthlyAmount("19720101-1234", 0, 100, 100)); // 47-åring ska få fullt lån
  }

  /**
    Test relaterade till studietakt
  */

  @Test
  public void ID201() { // studera minst halvtid för bidrag. Testar på 47-åring så jag kan bortse från lånet.
    assertEquals(0, pay.getMonthlyAmount("19720101-1234", 0, 49, 100)); // pluggar < 50%, förväntas 0kr i bidrag
  }

  @Test
  public void ID202() {
    assertEquals(4960, pay.getMonthlyAmount("19880101-1234", 0, 50, 100)); // pluggar 50%, förväntas 1396kr i bidrag
    assertEquals(4960, pay.getMonthlyAmount("19880101-1234", 0, 99, 100)); // pluggar 99%, förväntas 1396kr i bidrag
  }

  @Test
  public void ID203() { // studerar minst 50% och upp till 100%. Testar på 47-åring så jag kan bortse från lånet.
    assertEquals(2816, pay.getMonthlyAmount("19720101-1234", 0, 100, 100)); // pluggar 100%, förväntas 2816kr i bidrag
  }

  @Test
  public void ID301() { // Fulltidsstuderande får tjäna upp till 85813kr och fortfarande på lån och bidrag.
    assertEquals(9904, pay.getMonthlyAmount("19930918-8317", 85813, 100, 100)); // tjänar exakt på gränsen och förväntas få fullt lån och bidrag
    assertEquals(0, pay.getMonthlyAmount("19930918-8317", 85814, 100, 100)); // tjänar en krona över gränsen och förväntas få 0kr i lån och bidrag
  }

  @Test
  public void ID302() { // Deltidsstuderande får tjäna upp till 128722kr och fortfarande på lån och bidrag.
    assertEquals(4960, pay.getMonthlyAmount("19930918-8317", 128722, 50, 100)); // tjänar exakt på gränsen och förväntas få halvt lån och bidrag
    assertEquals(0, pay.getMonthlyAmount("19930918-8317", 128723, 50, 100)); // tjänar en krona över gränsen och förväntas få 0kr i lån och bidrag
  }

  @Test
  public void ID401() { // Student måste ha en completion ratio på minst 50 för att få något lån och bidrag
    assertEquals(9904, pay.getMonthlyAmount("19930918-8317", 0, 100, 50)); // klarade kurser exakt på gränsen och förväntas få fullt lån och bidrag
    assertEquals(0, pay.getMonthlyAmount("19930918-8317", 0, 100, 49)); // klarade 1% för få kurser och förväntas få 0kr i lån och bidrag
  }

  @Test
  public void ID501() { // Student som får helt lån men inget bidrag
    int full = pay.getMonthlyAmount("19930918-8317", 0, 100, 100);
    int subsid = pay.getMonthlyAmount("19700918-8317", 0, 100, 100);
    assertEquals(7088, full - subsid);
  }

  @Test
  public void ID502() { // Student som bara får fullt bidrag men inget lån
    int subsid = pay.getMonthlyAmount("19700918-8317", 0, 100, 100);
    assertEquals(2816, subsid);
  }

  @Test
  public void ID503() { // Student som får halvt lån men inget bidrag
    int half = pay.getMonthlyAmount("19930918-8317", 0, 99, 100);
    assertEquals(4960, half);
    int subsid = pay.getMonthlyAmount("19700918-8317", 0, 99, 100);
    assertEquals(3564, half - subsid);
  }

  @Test
  public void ID504() { // Student som bara får halvt bidrag men inget lån
    int subsid = pay.getMonthlyAmount("19700918-8317", 0, 99, 100);
    assertEquals(1396, subsid);
  }

  @Test
  public void ID505() { // Student kan visst få annat än fullt lån och bidrag
    int notFull = pay.getMonthlyAmount("19930918-8317", 0, 99, 100);
    assertNotSame(7088, notFull);
    assertNotSame(7088 + 1396, notFull);
    assertNotSame(7088 + 2816, notFull);
  }

  @Test
  public void ID506() { // testar att nästa utbetalning är sista vardagen i månaden. Funktionen bortser från högtider och sånt.
    assertEquals("20190430", pay.getNextPaymentDay()); // förväntar ge rätt
    assertNotSame("20190429", pay.getNextPaymentDay()); // en dag för tidigt
    assertNotSame("20190501", pay.getNextPaymentDay()); // en dag för sent.
  }


}
